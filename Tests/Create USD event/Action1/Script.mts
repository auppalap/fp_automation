﻿ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"

Dim APName1,Duration,Competence,Machine,Sequence,Materials,Engineers,URLEdit,StandByCat
Dim USDEventName,StartTime,RelativeOffset,Notes,APName2,Duration2,RelativeOffset2,Notes2,StandByCat2,EndTime

USDEventName = "USDAuto"&Now()
USDEventName = Replace(USDEventName,"/","")
USDEventName = Replace(USDEventName,":","")

APName1 = "AP1"&Now()
APName1 = Replace(APName1,"/","")
APName1 = Replace(APName1,":","")

APName2 = "AP2"&Now()
APName2 = Replace(APName2,"/","")
APName2 = Replace(APName2,":","")

Duration = fn_getdatatablevalue("Duration")
Competence = fn_getdatatablevalue("Competence")
Machine = fn_getdatatablevalue("Machine")
Sequence = fn_getdatatablevalue("Sequence")
Materials = fn_getdatatablevalue("Materials")
Engineers = fn_getdatatablevalue("Engineers")
URLEdit = fn_getdatatablevalue("URLEdit")
StandByCat = fn_getdatatablevalue("StandByCat")
StartTime = fn_getdatatablevalue("StartTime")
RelativeOffset = fn_getdatatablevalue("RelativeOffset")
Notes = fn_getdatatablevalue("Notes")
Duration2 = fn_getdatatablevalue("Duration2")
RelativeOffset2 = fn_getdatatablevalue("RelativeOffset2")
Notes2 = fn_getdatatablevalue("Notes2")
StandByCat2 = fn_getdatatablevalue("StandByCat2")
EndTime = fn_getdatatablevalue("EndTime")

Call fn_FP_Login()
Call fn_CreateUSDEvent(USDEventName,Machine,StartTime,EndTime,URLEdit,APName1,Competence,Duration,RelativeOffset,Sequence,Materials,Engineers,Notes,StandByCat,APName2,Duration2,RelativeOffset2,Notes2,StandByCat2)
Call fn_VerifytheUSDEventontheGantChart(USDEventName)

SystemUtil.CloseProcessByName "MSedge.exe"

