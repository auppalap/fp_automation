﻿

'Load Framework functions
ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"

 
'Web login
call fn_IBP_Web_Login

'Schedule the Job
Call fn_IBP_Scheduling_job(fn_getdatatablevalue("Job_Name"))

'ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"

'Search Job for status 
strStatus = fn_IBP_JobSerach_Status(fn_getdatatablevalue("Job_Name"))
'fn_IBP_JobSerach_Status(fn_getdatatablevalue("Job_Name"))
if strStatus <> "Finished" then
Reporter.ReportEvent micFail, "Not validating IBP Job" ,"Due to Job status is Failed in schedule"
exitrun
End If

Reporter.ReportEvent micPass,"Job passed","Job Passed"

'ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"
Call fn_Open_IBP_Excel(fn_getdatatablevalue("IBP_Addin_Connection_Name"))

'Open Excel Report from Favorites
Call Open_Excel_IBP_Report(fn_getdatatablevalue("Excel_Report_Name"))

ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"
Script_Excution_Start_Time = Date()&":"&Now()
'Verification of Different tabs of the report
Call Verify_Dataload_checks_Baseline_Holiday_Tab()
Call Verify_Dataload_checks_Baseline_AKF_Tab()

Call Verify_Dataload_checks_Baseline_APR_eq_PXX() ' Failed due to sum obj...

Call Verify_Dataload_checks_Baseline_PWO_notEq_pxx()

Call Verify_Dataload_checks_Baseline_PWO_Eq_pxx()

'ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"
 
Call Verify_Dataload_checks_Baseline_CPI_KF()

'Send report to Results folder
Call fn_Export_ResultSummary_Report()

'Send report to email
Call fn_Send_ResultsExcel_Email(Script_Excution_Start_Time)







