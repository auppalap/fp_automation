﻿'Load Framework functions
ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"

'Read output of Verification_Dataload_Checks_Baselines_Report script log for job run status pass or fail
strJobCompStatus=fn_Read_Values_From_File("Verification_Dataload_Checks_Baselines_Report")

If lcase(strJobCompStatus)="fail" or lcase(strJobCompStatus)<>"pass" Then
	reporter.ReportEvent micFail,"Job run status","Job run status is failed or not available"
	exitrun
End If

'Open IBP excel
Call fn_Open_IBP_Excel(fn_getdatatablevalue("IBP_Addin_Connection_Name"))

'Open IBP report From MDT favorites
Call fn_Open_Excel_IBP_Report_MDT(fn_getdatatablevalue("Job_Name"))

'Compare offset tabs
Call fn_Compare_ExcelSheets("A2","Offset_Base;Offset_EP;Offset_CP")

'Compare Obj tabs
Call fn_Compare_ExcelSheets("A2","Obj_Base;Obj_EP;Obj_CP")

'Compare OpNr tabs
Call fn_Compare_ExcelSheets("A2","OpNr_Base;OpNr_EP;OpNr_CP")

'Compare Loc.Testing Tabs
Call fn_Compare_ExcelSheets("A2","Loc.Testing_EP;Loc.Testing_CP")

'Compare Prod.Loc.Testing Tabs
Call fn_Compare_ExcelSheets("A2","Prod.Loc.Testing_EP;Prod.Loc.Testing_CP")





