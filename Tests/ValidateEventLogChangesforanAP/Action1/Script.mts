﻿ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"

Dim APName1,Duration,Competence,Machine,Sequence,Materials,Engineers,URLEdit,StandByCat,UpdatedAPName


APName = "AP"&Now()
APName = Replace(APName,"/","")
APName = Replace(APName,":","")

UpdatedAPName = APName&"UPDATE"

Duration = fn_getdatatablevalue("Duration")
Competence = fn_getdatatablevalue("Competence")
Machine = fn_getdatatablevalue("Machine")
Sequence = fn_getdatatablevalue("Sequence")
Materials = fn_getdatatablevalue("Materials")
Engineers = fn_getdatatablevalue("Engineers")
URLEdit = fn_getdatatablevalue("URLEdit")
StandByCat = fn_getdatatablevalue("StandByCat")

Call fn_FP_Login()
Call fn_CreateStandaloneActionPlan(APName,Duration,Competence,Machine,Sequence,Materials,Engineers,URLEdit,StandByCat)
Call fn_ValidateEventLoghangesforanAP(APName,UpdatedAPName)

SystemUtil.CloseProcessByName "MSedge.exe"



'Browser("Login").Page("Fleet Planner - Fleet").WebEdit("Action Plan name").SetTOProperty "value",APName
'Browser("Login").Page("Fleet Planner - Fleet").WebElement("Gann ChartElement").SetTOProperty "Innertext", ServiceEventName&".*"
