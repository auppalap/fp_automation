﻿If GBL_Iteration = 1 Then
	ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"
	GBL_Iteration = 2
End If

Dim UserRole,FullName,UserName,NewPassword,ConfirmPwd,Language,Timezone,WP_URL,TestUserCity,TestUserCustomer,TestUserFab,TestUserPlatform


UserRole = fn_getdatatablevalue("UserRole")
FullName = fn_getdatatablevalue("FullName")
UserName = fn_getdatatablevalue("UserName")
NewPassword = fn_getdatatablevalue("NewPassword")
ConfirmPwd = fn_getdatatablevalue("ConfirmPwd")
Language = fn_getdatatablevalue("Language")
Timezone = fn_getdatatablevalue("Timezone")
FP_URL = fn_getdatatablevalue("FP_URL")
TestUserCity = fn_getdatatablevalue("TestUserCity")
TestUserCustomer = fn_getdatatablevalue("TestUserCustomer")
TestUserFab = fn_getdatatablevalue("TestUserFab")
TestUserPlatform = fn_getdatatablevalue("TestUserPlatform")

Call fn_FP_Login()

Browser("Login").Page("Fleet Planner - Fleet").Link("User Management").Click
Browser("Login").Page("Fleet Planner - Fleet").WebButton("New local user").Click

Browser("Login").Page("Fleet Planner - Fleet").WebEdit("TestUserName").Set UserName
Browser("Login").Page("Fleet Planner - Fleet").WebList("TestUserLanguage").Select Language
Browser("Login").Page("Fleet Planner - Fleet").WebList("TestUserTimeZone").Select Timezone

Browser("Login").Page("Fleet Planner - Fleet").WebEdit("TestUserFullName").Set FullName

Browser("Login").Page("Fleet Planner - Fleet").WebEdit("TestUserNewPwd").Set NewPassword
Browser("Login").Page("Fleet Planner - Fleet").WebEdit("TestUserConfirmPwd").Set ConfirmPwd

Browser("Login").Page("Fleet Planner - Fleet").WebEdit("TestUserRole").Click
Wait 2

Select Case UserRole

      Case "Engineer"         
		Browser("Login").Page("Fleet Planner - Fleet").WebElement("Engineer").Click
		Browser("Login").Page("Fleet Planner - Fleet").WebButton("TestUserRoleSelect").Click

      Case "FleetManager"
      	Browser("Login").Page("Fleet Planner - Fleet").WebElement("FleetManager").Click
      	Browser("Login").Page("Fleet Planner - Fleet").WebButton("TestUserRoleSelect").Click
      	
      Case "FunctionalAdministrator"         
		Browser("Login").Page("Fleet Planner - Fleet").WebElement("FunctionalAdministrator").Click
		Browser("Login").Page("Fleet Planner - Fleet").WebButton("TestUserRoleSelect").Click
		
      Case "Planner"
      	Browser("Login").Page("Fleet Planner - Fleet").WebElement("Planner").Click
      	Browser("Login").Page("Fleet Planner - Fleet").WebButton("TestUserRoleSelect").Click
      	
      Case "TechLead"         
		Browser("Login").Page("Fleet Planner - Fleet").WebElement("TechLead").Click
		Browser("Login").Page("Fleet Planner - Fleet").WebButton("TestUserRoleSelect").Click
		
      Case "ViewOnly"
      	Browser("Login").Page("Fleet Planner - Fleet").WebElement("ViewOnly").Click
      	Browser("Login").Page("Fleet Planner - Fleet").WebButton("TestUserRoleSelect").Click
      
End Select

Browser("Login").Page("Fleet Planner - Fleet").WebButton("TestUserSave").highlight
Browser("Login").Page("Fleet Planner - Fleet").WebButton("TestUserSave").Click
Wait 10

Browser("Login").Page("Fleet Planner - Fleet").Link("Fleet Permissions").Click
Wait 5

Browser("Login").Page("Fleet Planner - Fleet").WebList("TestUser_Customer").Select TestUserCustomer
Wait 2
Browser("Login").Page("Fleet Planner - Fleet").WebList("TestUser_City").Select TestUserCity
Wait 2
Browser("Login").Page("Fleet Planner - Fleet").WebList("TestUser_Fab").Select TestUserFab
wait 2
Browser("Login").Page("Fleet Planner - Fleet").WebList("TestUser_Platform").Select TestUserPlatform
Wait 10

Call getFieldObject(FullName)

Browser("Login").Page("Fleet Planner - Fleet").WebButton("MovetoSubscribelist").Click
Wait 5
SystemUtil.CloseProcessByName "MSedge.exe"



SystemUtil.Run "MSedge.exe", FP_URL
Wait 2

If Browser("Login").Page("Login").Link("Sign in with other credentials").Exist Then
	Browser("Login").Page("Login").Link("Sign in with other credentials").Click
End If
Wait 2

Browser("Login").Page("Login").WebEdit("User name").Set UserName
Browser("Login").Page("Login").WebEdit("Password").Set NewPassword
Browser("Login").Page("Login").WebButton("Sign in").Click
wait 10

If Browser("Login").Page("Fleet Planner - Fleet").WebElement("Currently no machines").Exist Then
	
	Browser("Login").Page("Fleet Planner - Fleet").WebButton("NoMachinesWarningOK").Click
	Wait 2
	
	Browser("Login").Page("Fleet Planner - Fleet").WebElement("ManageFleetUnCheckedbtn").Click
	Browser("Login").Page("Fleet Planner - Fleet").WebElement("6465_ManageFleetUncheckedBtn").Click
	Browser("Login").Page("Fleet Planner - Fleet").WebElement("6817_ManageFleetUncheckedBtn").Click
	Browser("Login").Page("Fleet Planner - Fleet").WebElement("9739_ManageFleetUncheckedBtn").Click
	Browser("Login").Page("Fleet Planner - Fleet").WebElement("ES26_ManageFleetUncheckedBtn").Click
	
	Browser("Login").Page("Fleet Planner - Fleet").WebButton("SaveMyFleet").Click
	Wait 5
	Browser("Login").Page("Fleet Planner - Fleet").WebElement("Please refresh your browser").Click
	Browser("Login").Page("Fleet Planner - Fleet").WebButton("BrowserRefreshOK").Click
	Browser("Login").Refresh
	Wait 5

End If

Set Obj = Description.Create
Obj("micclass").value = "WebButton"
Obj("Class").Value = "btn dropdown-toggle dropdown-button btn-default btn-sm"
Obj("innertext").Value = FullName

If Browser("Login").Page("Fleet Planner - Fleet").WebButton(Obj).Exist Then
	Reporter.ReportEvent micPass,"Verify the Test User Creation","Test User is created successfully"
	Call CaptureScreen
Else
	Reporter.ReportEvent micfail,"Verify the Test User Creation","Test User is not created"
	Call CaptureScreen
End If
	
SystemUtil.CloseProcessByName "MSedge.exe"



'-------------------------------------------------------------------------------

'
'
'Dim Str
'Str = "test123"
'Set obj1 = getFieldObject(Str)
'obj1.highlight
'obj1.click
'Public Function getFieldObject(strFieldname)
'  
'Set objDesc=description.Create
'objDesc("micclass").value="WebCheckBox"
'objDesc("html tag").value="INPUT"
''objDesc("xpath").RegularExpression =True
'Set obj=Browser("Login").Page("Fleet Planner - Fleet").ChildObjects(objDesc)
'
'For i = 1 To obj.count-1 Step 1
'    if instr(obj(i).getroproperty("Xpath"),strFieldname)>0 then
'       set getFieldObject=obj(i)
'        Exit function
'     End If
'Next

'End Function

'Dim strFieldname
'strFieldname = "arrchang"
'Set obj1 = getFieldObject(arrchang)
'obj1.highlight
'obj1.click

'Browser("Login").Page("Fleet Planner - Fleet").WebButton("TestUserSave").highlight
'Browser("Login").Page("Fleet Planner - Fleet").WebButton("TestUserSave").Click
