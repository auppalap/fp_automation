﻿ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"
Dim ServiceEventName,Machine,StartTime,URLEdit,APName1,Competence,Duration,RelativeOffset,Sequence,Materials,Engineers,Notes,StandByCat,APName2,Duration2,RelativeOffset2,Notes2,StandByCat2

ServiceEventName = "UATest"&Now()
ServiceEventName = Replace(ServiceEventName,"/","")
ServiceEventName = Replace(ServiceEventName,":","")

APName1 = "AUTOAP1"&Now()
APName1 = Replace(APName1,"/","")
APName1 = Replace(APName1,":","")

APName2 = "AUTOAP2"&Now()
APName2 = Replace(APName2,"/","")
APName2 = Replace(APName2,":","")

Machine = fn_getdatatablevalue("Machine")
StartTime = fn_getdatatablevalue("StartTime")
URLEdit = fn_getdatatablevalue("URLEdit")
Competence = fn_getdatatablevalue("Competence")
Duration = fn_getdatatablevalue("Duration")
RelativeOffset = fn_getdatatablevalue("RelativeOffset")
Sequence = fn_getdatatablevalue("Sequence")
Materials = fn_getdatatablevalue("Materials")
Engineers = fn_getdatatablevalue("Engineers")
Notes = fn_getdatatablevalue("Notes")
StandByCat = fn_getdatatablevalue("StandByCat")
Duration2 = fn_getdatatablevalue("Duration2")
RelativeOffset2 = fn_getdatatablevalue("RelativeOffset2")
Notes2 = fn_getdatatablevalue("Notes2")
StandByCat2 = fn_getdatatablevalue("StandByCat2")

Call fn_FP_Login()
Call fn_CreateServiceEventwithMultipleAPs(ServiceEventName,Machine,StartTime,URLEdit,APName1,Competence,Duration,RelativeOffset,Sequence,Materials,Engineers,Notes,StandByCat,APName2,Duration2,RelativeOffset2,Notes2,StandByCat2)
Call fn_MoveAPToBacklogUntilSEIsRemoved(ServiceEventName,APName1,APName2)
SystemUtil.CloseProcessByName "MSedge.exe"

'Public Function fn_MoveAPToBacklogUntilSEIsRemoved(ServiceEventName,APName1,APName2)
'	
'Dim EventChk
'If Browser("Login").Page("Fleet Planner - Fleet").WebButton("Show labels").Exist Then
'		Browser("Login").Page("Fleet Planner - Fleet").WebButton("Show labels").Click
'End If	
'
'Browser("Login").Page("Fleet Planner - Fleet").WebList("View").Select "Machine View"
'	
'Browser("Login").Page("Fleet Planner - Fleet").WebElement("Gann ChartElement").SetTOProperty "Innertext", ServiceEventName&".*"
'Setting.WebPackage("ReplayType") = 2
'	
'If Browser("Login").Page("Fleet Planner - Fleet").WebElement("Gann ChartElement").Exist Then
'	
'	Browser("Login").Page("Fleet Planner - Fleet").WebElement("Gann ChartElement").highlight
'	Browser("Login").Page("Fleet Planner - Fleet").WebElement("Gann ChartElement").FireEvent "onmouseover"
'	Call CaptureScreen
'	wait 10
'	Browser("Login").Page("Fleet Planner - Fleet").WebElement("Gann ChartElement").Click 5,5
'	Wait 2
'	Browser("Login").Page("Fleet Planner - Fleet").WebElement("Gann ChartElement").Click 5,5
'	Setting.WebPackage("ReplayType") = 1
'	Call CaptureScreen	
'	EventChk = Browser("Login").Page("Fleet Planner - Fleet").WebEdit("Service Event Name").GetROProperty("value")
'
'	If InStr(EventChk,ServiceEventName) Then
'				
'		Browser("Login").Page("Fleet Planner - Fleet").WebButton("Action Plans").Click
'			
'		'ActionPlan 1
'		Set obj1 = Description.Create
'		obj1("micclass").value = "WebTable"
'		obj1("innertext").value = ".*"&fn_getdatatablevalue("APName1")&".*"
'		obj1("html tag").value = "TABLE"
'		obj1("name").value = ".*"&fn_getdatatablevalue("APName1")&".*"
'			
'		'ActionPlan 2
'		Set obj2 = Description.Create
'		obj2("micclass").value = "WebTable"
'		obj2("innertext").value = ".*"&fn_getdatatablevalue("APName2")&".*"
'		obj2("html tag").value = "TABLE"
'		obj2("name").value = ".*"&fn_getdatatablevalue("APName2")&".*"
'		
'		If Browser("Login").Page("Fleet Planner - Fleet").WebTable(obj1).Exist Then
'			
'			Browser("Login").Page("Fleet Planner - Fleet").WebTable(obj1).Highlight
'			Set objLink = Browser("Login").Page("Fleet Planner - Fleet").WebTable(obj1).ChildItem(1,3,"WebButton",0)
'			objLink.click
'			Call CaptureScreen
'			Browser("Login").Page("Fleet Planner - Fleet").WebButton("Move to Backlog").highlight
'			Browser("Login").Page("Fleet Planner - Fleet").WebButton("Move to Backlog").Click
'			Browser("Login").Page("Fleet Planner - Fleet").WebButton("MovetoBacklogConfirm").Click
'			wait 2
'				
'		Else
'			Reporter.ReportEvent micFail, "Verify Action Plan1","ActionPlan "&fn_getdatatablevalue("APName1")&" is not displayed"			
'		End If
'			
'		If Browser("Login").Page("Fleet Planner - Fleet").WebTable(obj2).Exist Then
'			
'			Browser("Login").Page("Fleet Planner - Fleet").WebTable(obj2).Highlight
'			Set objLink = Browser("Login").Page("Fleet Planner - Fleet").WebTable(obj1).ChildItem(1,3,"WebButton",0)
'			objLink.click
'			Call CaptureScreen
'			Browser("Login").Page("Fleet Planner - Fleet").WebButton("Move to Backlog").highlight
'			Browser("Login").Page("Fleet Planner - Fleet").WebButton("Move to Backlog").Click
'			Browser("Login").Page("Fleet Planner - Fleet").WebButton("MovetoBacklogConfirm").Click
'			wait 2
'			Call CaptureScreen
'				
'		Else
'			Reporter.ReportEvent micFail, "Verify Action Plan2","ActionPlan "&fn_getdatatablevalue("APName2")&" is not displayed"			
'		End If
'		
'		If Not Browser("Login").Page("Fleet Planner - Fleet").WebElement("Gann ChartElement").Exist Then
'			
'			Reporter.ReportEvent micpass, "Verify SE Deletion","All the APs moved to Backlog until ServiceEvent deleted"
'		Else
'			Reporter.ReportEvent micfail, "Verify SE Deletion","Action Plans are not moved to Backlog"
'			
'		End If
'					
'	Else
'		Reporter.ReportEvent micFail, "Verify the ServiceEvent & ActionPlan Selection","Service Event is not selected"
'	End If
'		
'Else
'	
'	Reporter.ReportEvent micFail, "Verify the Newly Created ServiceEvent", "Object does not found"
'	
'End If
'	
'End Function





'Browser("Login").Page("Fleet Planner - Fleet_2").WebEdit("RelativeOffSet").Set "10"

